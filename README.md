This repository is intended for people that want a fresh and set-up python project.
It is intended to be cloned and customized for your own needs.

This project is configured for a normal git flow workflow: 2 common branches `dev` and `master`
intended for the most up-to-date code and releases respectively. Every other branch
is assumed as a temporary branch that will need merging in `dev` (and `master` for hotfix).