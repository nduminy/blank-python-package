# syntax=docker/dockerfile:1

FROM python:3.11.0-slim

RUN apt-get update && apt-get install -y build-essential libssl-dev libxml2-dev zlib1g-dev

WORKDIR /src

COPY ./ ./

RUN pip install .

WORKDIR /src
