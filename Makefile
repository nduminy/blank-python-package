# --
SHELL := bash
.ONESHELL:
.SHELLFLAGS := -o errexit -o nounset -o pipefail -c
.DELETE_ON_ERROR:
MAKEFLAGS += --warn-undefined-variables
MAKEFLAGS += --no-builtin-rules

# --
ISORT_OPTS=--profile black --combine-star --use-parentheses -m VERTICAL_HANGING_INDENT -w 79
FLAKE_OPTS=--max-line-length=79 --ignore=E203,W503
SRCS:=src tests
# --
.PHONY: install build isort-check isort-apply flake8 black-check black-apply lint lint-apply doc api

install:
	pip install -r requirements.txt

build:
	python -m build

isort-check:
	isort ${ISORT_OPTS} --check --diff ${SRCS}

isort-apply:
	isort --atomic ${ISORT_OPTS} ${SRCS}

flake8:
	flake8 ${FLAKE_OPTS} ${SRCS}

black-check:
	black --line-length 79 --check ${SRCS}

black-apply:
	black --line-length 79 ${SRCS}

lint: flake8 isort-check black-check

lint-apply: isort-apply black-apply

api:
	cd doc
	sphinx-apidoc -fMeT -d 1 -o ./api/ ../src/blank_project

doc: api
	cd doc
	$(MAKE) html

.PHONY: test clean coverage coverage-report coverage-report-html cov-html

test:
	python -m pytest

coverage:
	coverage run --source src,tests --branch -m pytest

coverage-check: coverage
	coverage report --fail-under=100

coverage-report: coverage
	coverage report

coverage-report-html cov-html: coverage
	coverage html

clean:
	-find . -type d -name '__pycache__' -exec rm --recursive --force {} +
	coverage erase
	rm -Rf doc/html doc/api doc/doctrees